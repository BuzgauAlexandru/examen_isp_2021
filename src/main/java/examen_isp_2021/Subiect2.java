package examen_isp_2021;

import javax.naming.ServiceUnavailableException;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Subiect2 extends JFrame {

    JButton buton;
    JTextField text1, text2;


    Subiect2() {
        setTitle("Subiect 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 300);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 20;

        text1 = new JTextField();
        text1.setBounds(50, 50, width, height);

        text2 = new JTextField();
        text2.setBounds(50, 100, width, height);

        buton = new JButton("Click");
        buton.setBounds(50, 150, width, height);
        buton.addActionListener(new Subiect2.TratareButon());

        add(text1);add(text2);add(buton);

    }


    class TratareButon implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            String prop = text1.getText();
            int car = 0;

            for (int i = 0; i < prop.length(); i++) {
                if (prop.charAt(i) != ' ')
                    car++;
            }

            text2.setText(String.valueOf(car));


        }
    }

        public static void main(String[] args) {
            new Subiect2();
        }



}